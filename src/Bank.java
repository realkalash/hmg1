public class Bank extends Market {
    private double limitUAH;

    public Bank(String name, double courseUSD, double limitUAH) {
        super(name, courseUSD);
        this.limitUAH = limitUAH;
    }

    public double getLimitUAH() {
        return limitUAH;
    }

    public void setLimitUAH(double limitUAH) {
        this.limitUAH = limitUAH;
    }
}

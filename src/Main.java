import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Bank[] banks = Generator.GenerateBanks();
        Exchanger[] exchangers = Generator.GenerateExchangers();
        BlackMarket blackMarket = Generator.GenerateBlackMarket();

        System.out.println("Выберите кол-во суммы в грн для конвертации");

        Scanner scanner = new Scanner(System.in);
        int input = Integer.parseInt(scanner.nextLine());
        double output;

        for (Bank bank : banks) {
            if (input < bank.getLimitUAH()) {
                output = input / bank.getCourseUSD();
                System.out.println("После конвертации в " + bank.getName() + " это вышло " + output + "$");
            } else
                System.out.println("Конвертация невозможна");
        }
        for (Exchanger exchanger : exchangers) {
            if (input / exchanger.getCourseUSD() < exchanger.getLimitUSD()) {
                output = input / exchanger.getCourseUSD();
                System.out.println("После конвертации в " + exchanger.getName() + " это вышло " + output + "$");
            } else
                System.out.println("Конвертация невозможна");
        }

        System.out.println("После конвертации в " + blackMarket.getName() + " это вышло " + input / blackMarket.getCourseUSD() + "$");

    }
}

public class Market {
    private String name;
    private double courseUSD;

    public Market(String name, double courseUSD) {
        this.name = name;
        this.courseUSD = courseUSD;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCourseUSD() {
        return courseUSD;
    }

    public void setCourseUSD(double courseUSD) {
        this.courseUSD = courseUSD;
    }

}

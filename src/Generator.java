public class Generator {
    protected static Bank[] GenerateBanks() {
        Bank[] banks = new Bank[2];
        {
            Bank bank = new Bank("Privat24", 26.5d, 5000d);
            banks[0] = bank;
        }
        {
            Bank bank = new Bank("OshadBank", 27d, 10000d);
            banks[1] = bank;
        }

        return banks;
    }
    protected static Exchanger[] GenerateExchangers() {
        Exchanger[] exchangers = new Exchanger[2];
        {
            Exchanger exchanger = new Exchanger("First Exchenger", 25d, 250d);
            exchangers[0] = exchanger;
        }
        {
            Exchanger exchanger = new Exchanger("second Exchenger", 28d, 300d);
            exchangers[1] = exchanger;
        }
        return exchangers;
    }
    protected static BlackMarket GenerateBlackMarket() {

            BlackMarket blackMarket = new BlackMarket("BlackMarket", 25.5);

        return blackMarket;

    }
}

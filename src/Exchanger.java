public class Exchanger extends Market {

    double limitUSD;

    public Exchanger(String name, double courseUSD, double limitUSD) {
        super(name, courseUSD);
        this.limitUSD = limitUSD;
    }

    public double getLimitUSD() {
        return limitUSD;
    }
}
